let firsts = 0;
let seconds = 0;
let lastms = 00;

if (localStorage.getItem("savedTime") === null) {
  let counter = 0;
} else {
  counter = localStorage.getItem("savedTime");
}

let started = false;
let x = setInterval(function () {
  if (started == true) {
    counter++;
  }
  lastms = counter % 100;
  firsts = Math.floor(counter / 100) % 10;
  seconds = Math.floor(counter / 1000);
  document.getElementById("time").innerHTML = `${seconds}${firsts}:${lastms}`;
}, 1000);

function start() {
  started = true;
}

function pause() {
  started = false;
  if (typeof Storage !== "undefined") {
    localStorage.setItem("savedTime", counter);
  } else {
    document.getElementById("result").innerHTML =
      "Update your browser or install another software!";
  }
}

function reset() {
  counter = 0;
  localStorage.setItem("savedTime", counter);
}
